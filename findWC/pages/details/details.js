
Page({
    data:{
        num: 0,
    },
    onLoad: function(options){
        console.log(options)
        this.setData({
            num: options.num * 10
        })
    },
    bindViewTap: function(){
        wx.navigateBack({
          delta: 5
        })
    }
})