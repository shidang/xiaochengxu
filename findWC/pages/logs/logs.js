//logs.js

Page({
  data: {
    logsImg: '',
    logsTitle: '',
    num: 0
  },
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      // 獲取導航標題
      title: options.title,
      success: function(res){

      }
    }),

    //獲取數據
    this.setData({
      logsImg: options.img,
      logsTitle: options.title,
      num: Math.floor(Math.random()*30)
    })
  },
  onShareAppMessage: function(){
    //分享
    return {
      title: this.data.logsTitle,
      desc: '完美',
      path: 'path'
    }
  }
})
