//index.js
//获取应用实例
var app = getApp()
var winHeight = 0
var winWidth = 0

Page({
  data: {
    img:'/pages/image/123.png',
    dataArr:[{'left': 200,'top': 100,'title': '奢华打造马桶','img': '/pages/image/1.png'},
      {'left': 20,'top': 400,'title': '听雨轩','img': '/pages/image/2.png'},
      {'left': 540,'top': 440,'title': '观瀑亭','img': '/pages/image/3.png'},
      {'left': 240,'top': 880,'title': '流水型设计','img': '/pages/image/4.png'}]
    
  },
// 进入页面获取数据
  onLoad: function () {
    console.log('onLoad')
    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo(function(userInfo){
      //更新数据
      that.setData({
        userInfo:userInfo
      })
    })

    // 获取数据
    wx.getSystemInfo({
      success: function(res){
        console.log(res);
        winWidth = res.windowWidth;
        winHeight = res.windowHeight;
      },
      fail: function(err){
        console.log(err);
      }
    })
    
    // 使用 wx.createContext 获取绘图上下文 context
    var context = wx.createContext()
    context.arc(winWidth/2, winHeight/2,50,0,2*Math.PI,true)
    context.arc(winWidth/2, winHeight/2,100,0,2*Math.PI,true)
    context.arc(winWidth/2, winHeight/2,150,0,2*Math.PI,true)
    context.arc(winWidth/2, winHeight/2,200,0,2*Math.PI,true)
    context.arc(winWidth/2, winHeight/2,250,0,2*Math.PI,true)
    context.arc(winWidth/2, winHeight/2,300,0,2*Math.PI,true)

    context.setStrokeStyle('red')
    context.setLineWidth(1)
    context.stroke()

    // 调用 wx.drawCanvas，通过canvasId 指定在哪张画布绘制，通过actions指定绘制行为
    wx.drawCanvas({
      canvasId: 'radar',
      actions: context.getActions() //获取绘图动作数组
    })
  },

  onShareAppMessage: function(){
    //用户点击右上角分享
    return {
      title: '共享厕所', //分享标题
      desc: '一个萝卜一个坑', //分享描述
      path: "path" //分享路径
    }
  }
})
