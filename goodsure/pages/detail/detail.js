var WxParse = require('../../wxParse/wxParse.js');
var app = getApp();
var url = "https://www.goodsure.cn/cron/app_index.php?q=article_content&article_id=";

Page({
    data: {
        userInfo: {}
    }, 
    onLoad: function (options) {
        var that = this
        wx.request({
            url: url+options.id,
            data: {},
            success:function(res){
                var article = res.data.content;
                WxParse.wxParse('article', 'html', article, that, 5);
                var imgSrc = res.data.litpic;
                that.setData({
                    
                    imgSrc: imgSrc
                })
            }
        })

        // //调用应用实例的方法获取全局数据
        app.getUserInfo(function(userInfo){
          //更新数据
          that.setData({
            userInfo:userInfo
          })
        })
        
    
    },
    onShareAppMessage: function(){
        //用户点击右上角分享
        return {
          title: '果树财富', //分享标题
          desc: '投资人自己的网贷平台,上市系P2P理财平台', //分享描述
          path: "path" //分享路径
        }
    }
})