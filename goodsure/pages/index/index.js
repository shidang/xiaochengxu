//index.js
//获取应用实例
var app = getApp();
var redirect_url = '/pages/detail/detail?';
var url = "https://www.goodsure.cn/cron/app_index.php?q=article_list";
var page = 1;
var epage = 6;
var total_page;

// 获取数据的方法
var GetList = function(that){
    if(page >= total_page ){
      return false;
    }
    that.setData({
        hidden:false
    });
    wx.request({
        url:url,
        data:{
            page : page,
            epage : epage,
        },
        success:function(res){
            total_page = res.data.total_page;
            var list = that.data.list;
            for(var i = 0; i < res.data.list.length; i++){
                list.push(res.data.list[i]);
            }
            that.setData({
                list : list
            });
            page ++;
            that.setData({
                hidden:true
            });
        }
    });
}

Page({
  data: {
    hidden: true,
    imgUrls: [
      'https://www.goodsure.cn/data/upfiles/images/2016-03/15/381_scrollpic_14580052153.jpg',
      'https://www.goodsure.cn/data/upfiles/images/2016-03/15/381_scrollpic_14580045192.jpg',
      'https://www.goodsure.cn/data/upfiles/images/2016-03/15/381_scrollpic_14580044759.jpg'
    ],
    userInfo: {},
    indicatorDots: false,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    list: [],
  },
  detail: function(event) {
      console.log(event.currentTarget.id);
      wx.navigateTo({
        url: redirect_url+'id='+event.currentTarget.id
      });
  },
  onReachBottom: function () {  
    //上拉    
    var that = this  
    GetList(that)  
  },
  onLoad: function () {
    var that = this
    GetList(that) 
    //调用应用实例的方法获取全局数据
    app.getUserInfo(function(userInfo){
      //更新数据
      that.setData({
        userInfo:userInfo
      })
    })
  },
  onShareAppMessage: function(){
        //用户点击右上角分享
        return {
          title: '果树财富', //分享标题
          desc: '投资人自己的网贷平台,上市系P2P理财平台', //分享描述
          path: "path" //分享路径
        }
    }

})
