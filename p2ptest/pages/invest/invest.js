//index.js
//获取应用实例
var app = getApp()
Page({
    data: {
        tabArr: { 
           curHdIndex: 0, 
           curBdIndex: 0 
        }, 
        datappr: {},
        userInfo: {}
    },
    // tab切换
    tabSel: function(e){ 
        //获取触发事件组件的dataset属性 
        console.log(e);
        var _datasetId=e.target.dataset.id; 
        var _obj={}; 
        _obj.curHdIndex=_datasetId; 
        _obj.curBdIndex=_datasetId; 
        this.setData({ 
           tabArr: _obj 
        }); 
    }, 
    onLoad: function (options) {
        console.log('onLoad')
        var that = this
        // //调用应用实例的方法获取全局数据
        app.getUserInfo(function(userInfo){
          //更新数据
          that.setData({
            userInfo:userInfo
          })
        })

        that.setData({
            datappr: {
                id: options.id,
                name: "新手专享标",
                account: 50000,
                account_yes: 20000,
                apr: 17.2,
                time_limit: 1,
                spend: 78,
                most_account: 5000,
                borrow_type: 245,
            }
           
        })
    
    },

    onShareAppMessage: function(){
        //用户点击右上角分享
        return {
          title: '果树财富', //分享标题
          desc: '投资人自己的网贷平台,上市系P2P理财平台', //分享描述
          path: "path" //分享路径
        }
    }





})
