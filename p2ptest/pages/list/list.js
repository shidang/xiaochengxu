var url = "http://www.imooc.com/course/ajaxlist";
var page =0;
var page_size = 20;
var sort = "last";
var is_easy = 0;
var lange_id = 0;
var pos_id = 0;
var unlearn = 0;


// 获取数据的方法
var GetList = function(that){
    that.setData({
        hidden:false
    });
    wx.request({
        url:url,
        data:{
            page : page,
            page_size : page_size,
            sort : sort,
            is_easy : is_easy,
            lange_id : lange_id,
            pos_id : pos_id,
            unlearn : unlearn
        },
        success:function(res){
            //console.info(that.data.list);
            var list = that.data.list;
            for(var i = 0; i < res.data.list.length; i++){
                list.push(res.data.list[i]);
            }
            that.setData({
                list : list
            });
            page ++;
            that.setData({
                hidden:true
            });
        }
    });
} 
Page({  
  data: {  
    list: [],
    tabArr: { 
       tIndex: 0, 
       cIndex: 0 
    },   
  }, 
  // tab切换
  tab: function(e){ 
      //获取触发事件组件的dataset属性 
      console.log(e);
      var _datasetId=e.target.dataset.id; 
      var _obj={}; 
      _obj.tIndex=_datasetId; 
      _obj.cIndex=_datasetId; 
      this.setData({ 
         tabArr: _obj 
      }); 
  },  
  onLoad: function (options) {  
    // 页面初始化 options为页面跳转所带来的参数  
    var that = this  
    GetList(that)  
  },  
  onPullDownRefresh: function () {  
    //下拉  
    console.log("下拉");  
    page = 1;  
    this.setData({  
      list: [],  
    });  
    var that = this  
    GetList(that)  
  },  
  onReachBottom: function () {  
    //上拉  
    console.log("上拉")  
    var that = this  
    GetList(that)  
  }  
})  
