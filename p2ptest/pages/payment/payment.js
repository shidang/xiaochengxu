
//获取应用实例
var app = getApp()
Page({
    data: {
        datappr: {},
        userInfo: {}
    },
    formSubmit: function(e) {
        console.log('form发生了submit事件，携带数据为：', e.detail.value)
        wx.showModal({
          title: '果树财富',
          content: '投资成功或失败弹出',
          showCancel: false,
          success: function(res) {
            if (res.confirm) {
                wx.redirectTo({
                  url: '../account/account'
                })
            }
          }
        })

    },
    formReset: function() {
        console.log('form发生了reset事件')
        wx.navigateBack()

    },
    onLoad: function (op) {
        var that = this
        // //调用应用实例的方法获取全局数据
        app.getUserInfo(function(userInfo){
          //更新数据
          that.setData({
            userInfo:userInfo
          })
        })

        that.setData({
            datappr: {
                account: op.account,
                account_yes: op.account_yes
            }
        })




    },

    onShareAppMessage: function(){
        //用户点击右上角分享
        return {
          title: '果树财富', //分享标题
          desc: '投资人自己的网贷平台,上市系P2P理财平台', //分享描述
          path: "path" //分享路径
        }
    }


})
